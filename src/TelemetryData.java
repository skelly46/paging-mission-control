	public class TelemetryData {
		
		int satelliteId;
	    String severity;
	    String component;
	    String timestamp;
	    String simpleTime;
	    componentValues data;
	
		public TelemetryData(String telemetryIn)
		{
			String[] arrTelemetry = telemetryIn.split("\\|");
			String timestampIn = arrTelemetry[0];
			String satelliteIdIn = arrTelemetry[1];
			String redHighLimitIn = arrTelemetry[2];
			String yellowHighLimitIn = arrTelemetry[3];
			String yellowLowLimitIn = arrTelemetry[4];
			String redLowLimitIn = arrTelemetry[5];
			String rawValueIn = arrTelemetry[6];
			String componentIn = arrTelemetry[7];
			timestamp = timestampFormat(timestampIn);
			simpleTime = simpleTimeFormat(timestampIn);
			satelliteId = Integer.parseInt(satelliteIdIn);
			data = new componentValues(Integer.parseInt(redHighLimitIn), Integer.parseInt(yellowHighLimitIn), 
										Integer.parseInt(yellowLowLimitIn), Integer.parseInt(redLowLimitIn), 
										Double.parseDouble(rawValueIn));
			component = componentIn;
			severity = data.status;
		}
	
		public String toString()
		{
			String out;
			out = String.format(TelemetryConstants.TELEMETRY_FORMAT, 
								TelemetryConstants.SATELLITEID, satelliteId,
								TelemetryConstants.SEVERITY, severity,
								TelemetryConstants.COMPONENT, component,
								TelemetryConstants.TIMESTAMP, timestamp);
			return out;
		}
		
		public String timestampFormat(String timestampIn)
		{
			String timestampFormatted = "";
			String[] arrTimestampIn = timestampIn.split(" ");		
			String date = arrTimestampIn[0];
			String time = arrTimestampIn[1];
			String year = date.substring(0, 4);
			String month = date.substring(4, 6);
			String day = date.substring(6,8);
			timestampFormatted = String.format(TelemetryConstants.TIMESTAMP_FORMAT,year,month,day,time);			
			
			return timestampFormatted;
		}

		public String simpleTimeFormat(String timestampIn)
		{
			String timestampFormatted = "";
			String[] arrTimestampIn = timestampIn.split(" ");		
			String date = arrTimestampIn[0];
			String time = arrTimestampIn[1];
			String year = date.substring(0, 4);
			String month = date.substring(4, 6);
			String day = date.substring(6,8);
			timestampFormatted = String.format(TelemetryConstants.SIMPLETIME_FORMAT,year,month,day,time);			
			
			return timestampFormatted;
		}
		
		
		public class componentValues{
			double redHighLimit;
			double redLowLimit;
			double yellowHighLimit;
			double yellowLowLimit;
			double rawValue;
			String status;
			
			public componentValues(double redHighLimitIn, double yellowHighLimitIn, double yellowLowLimitIn, double redLowLimitIn, double rawValueIn)
			{
				redHighLimit = redHighLimitIn;
				yellowHighLimit = yellowHighLimitIn;
				yellowLowLimit = yellowLowLimitIn;
				redLowLimit = redLowLimitIn;
				rawValue = rawValueIn;
				status = CalculateStatus();
			}
			
			private String CalculateStatus()
			{
				
				String status = "";
	
				if(rawValue > redHighLimit)
				{
					status = TelemetryConstants.RH;
				}
				else if (rawValue > yellowHighLimit)
				{
					status = TelemetryConstants.YH;
				}
				else if (rawValue < yellowHighLimit && rawValue > yellowLowLimit)
				{
					status = TelemetryConstants.G;
				}
				else if (rawValue < redLowLimit)
				{
					status = TelemetryConstants.RL;
				}			
				else if (rawValue < yellowLowLimit)
				{
					status = TelemetryConstants.YL;
				}			
				
				return status;
			}
		}
		
		public static class TelemetryConstants
		{			
			// components
			static String TSTAT = "TSTAT";
			static String BATT = "BATT";
			
			// statuses
			static String RH = "RED HIGH";
			static String RL = "RED LOW";
			static String G = "GOOD";
			static String YH = "YELLOW HIGH";
			static String YL = "YELLOW LOW";	
			
			// output formats
			static String SATELLITEID = "satelliteId";
			static String SEVERITY = "severity";
			static String COMPONENT = "component";
			static String TIMESTAMP = "timestamp";
			
			// input formats
			static String PROGRAM_TITLE = "Paging Mission Control!";
			static String USER_INPUT_REQUEST = "Enter ASCII text file path for telemetry data:";
			static String USER_INPUT_ERROR = "File not Found: %s\n"
											+ "i.e. C:\\temp\\telemetry.txt";

			// string formats
			static String TIMESTAMP_FORMAT = "%s-%s-%sT%sZ";
			static String SIMPLETIME_FORMAT = "%s-%s-%s %s";
			static String TELEMETRY_FORMAT = "\"%s\": %d,\n"
											+ "\"%s\": \"%s\",\n"
											+ "\"%s\": \"%s\",\n"
											+ "\"%s\": \"%s\"";
			static String SIMPLEDATA_FOMRAT = "yyyy-MM-dd hh:mm:ss.SSS";
			
			// alert parameters
			static int ALERT_TIME_INTERVAL = 300000;
			static int NUM_OF_ERRORS_FOR_ALERT = 3;
			
		}
	}
