import java.io.*;
import java.text.*;
import java.util.*;

public class PagingMissionControlModel {

	public ArrayList<TelemetryData> GetTelemetryData()
	{
		boolean validUserFilePath;
		Scanner userIn = new Scanner(System.in);
		String telemetryFilePath;
		File telemetryFile;
		ArrayList<TelemetryData> arrTelemetryData = new ArrayList<TelemetryData>();
		Scanner fileIn;
		
		// Repeat user input file until valid path
		do{
			validUserFilePath = false;	
			// Request user to enter in telemetry data		
			System.out.println(TelemetryData.TelemetryConstants.USER_INPUT_REQUEST);
			
			// Check file path is valid
			telemetryFilePath = userIn.nextLine();
			
//			//DEBUG to skip entering path name
//			telemetryFilePath = "C:\\Temp\\Test.txt";
			
			try {
				telemetryFile = new File(telemetryFilePath);
				fileIn = new Scanner(telemetryFile);
				String currLine;
				while(fileIn.hasNextLine())
				{
					currLine = fileIn.nextLine();

					TelemetryData currTelemetryData = new TelemetryData(currLine);
					arrTelemetryData.add(currTelemetryData);
				}
				fileIn.close();
				
				
			}
			catch (FileNotFoundException e) {
				String errorMessage = String.format(TelemetryData.TelemetryConstants.USER_INPUT_ERROR, telemetryFilePath);
				System.out.println(errorMessage);
				validUserFilePath = true;
			}
		} while (validUserFilePath);
		
		userIn.close();
		
		return arrTelemetryData;
	}

	public String ArrTelemetryDataToString(ArrayList<TelemetryData> arrTelemetryDataIn)
	{
		int arrTelemetrySize = arrTelemetryDataIn.size();
		String telemetryData = "";		
		telemetryData += "[\n";

		for (int arrTelemetryDataLoop = 0; arrTelemetryDataLoop < arrTelemetrySize; arrTelemetryDataLoop++ )
		{
			String currTelemetryDataFormatted = arrTelemetryDataIn.get(arrTelemetryDataLoop).toString().replaceAll("\n", "\n\t\t");

			if(arrTelemetryDataLoop != arrTelemetrySize - 1)
			{
				telemetryData += String.format("\t{\n"
												+ "\t\t%s\n"
												+ "\t},\n", currTelemetryDataFormatted);
			}
			else
			{
				telemetryData += String.format("\t{\n"
												+ "\t\t%s\n"
												+ "\t}\n", currTelemetryDataFormatted);				
			}
			
		}

		telemetryData += "]";
		
		return telemetryData;		
	}
	
	public ArrayList<TelemetryData> ArrTelemetryDataCheckAlerts(ArrayList<TelemetryData> arrTelemetryDataIn)
	{
		int arrTelemetryDataSize = arrTelemetryDataIn.size();
		ArrayList<TelemetryData> arrTelemetryAlerts = new ArrayList<TelemetryData>();
		SimpleDateFormat dateFormat = new SimpleDateFormat(TelemetryData.TelemetryConstants.SIMPLEDATA_FOMRAT);
				
		// Loop to find any red values
		for (int arrTelemetryDataLoop = 0; arrTelemetryDataLoop < arrTelemetryDataSize; arrTelemetryDataLoop++)
		{
			try {
				TelemetryData currTelemetryData = arrTelemetryDataIn.get(arrTelemetryDataLoop);
				long currTelemetryTimeMilli = dateFormat.parse(currTelemetryData.simpleTime).getTime();
				
				// If battery is red low
				if(currTelemetryData.severity.equals(TelemetryData.TelemetryConstants.RL) && currTelemetryData.component.equals(TelemetryData.TelemetryConstants.BATT))
				{					
					// Count number of errors occur
					int numOfErrors = 1;
					
					// Check if enough errors in alert time interval to cause alert
					for (int arrTelemetryDataErrorLoop = arrTelemetryDataLoop + 1; arrTelemetryDataErrorLoop < arrTelemetryDataSize; arrTelemetryDataErrorLoop++)
					{
						TelemetryData compTelemetryData = arrTelemetryDataIn.get(arrTelemetryDataErrorLoop);
						long compTelemetryTimeMilli = dateFormat.parse(compTelemetryData.simpleTime).getTime();
						long timeDelta = compTelemetryTimeMilli - currTelemetryTimeMilli;	

						// If same satellite and within 5 minutes of first red low
						if(compTelemetryData.severity.equals(TelemetryData.TelemetryConstants.RL) && compTelemetryData.component.equals(TelemetryData.TelemetryConstants.BATT) &&
								currTelemetryData.satelliteId == compTelemetryData.satelliteId && timeDelta < TelemetryData.TelemetryConstants.ALERT_TIME_INTERVAL)
						{
							numOfErrors += 1;
							// If at least 3 errors create alert
							if(numOfErrors >= TelemetryData.TelemetryConstants.NUM_OF_ERRORS_FOR_ALERT)
							{
								arrTelemetryAlerts.add(currTelemetryData);	//Add to alerts if battery is red low		
								// break since alert is triggered
								break;						
							}
						}
						else if(timeDelta > TelemetryData.TelemetryConstants.ALERT_TIME_INTERVAL)
						{
							// break if timeDelta is 5+ minutes
							break;
						}
					
					}					
				}

				// If thermostat is red high
				if(currTelemetryData.severity.equals(TelemetryData.TelemetryConstants.RH) && currTelemetryData.component.equals(TelemetryData.TelemetryConstants.TSTAT))
				{			
					// Count number of errors occur
					int numOfErrors = 1;
					
					// Check if enough errors in alert time interval to cause alert
					for (int arrTelemetryDataErrorLoop = arrTelemetryDataLoop + 1; arrTelemetryDataErrorLoop < arrTelemetryDataSize; arrTelemetryDataErrorLoop++)
					{
						TelemetryData compTelemetryData = arrTelemetryDataIn.get(arrTelemetryDataErrorLoop);
						long compTelemetryTimeMilli = dateFormat.parse(compTelemetryData.simpleTime).getTime();
						long timeDelta = compTelemetryTimeMilli - currTelemetryTimeMilli;	
						
						// If same satellite, thermostat is red high, and within 5 minutes of first red high
						if(compTelemetryData.severity.equals(TelemetryData.TelemetryConstants.RH) && compTelemetryData.component.equals(TelemetryData.TelemetryConstants.TSTAT) &&
								currTelemetryData.satelliteId == compTelemetryData.satelliteId && timeDelta < TelemetryData.TelemetryConstants.ALERT_TIME_INTERVAL)
						{
							
							numOfErrors += 1;
							// If at least 3 errors create alert
							if(numOfErrors >= TelemetryData.TelemetryConstants.NUM_OF_ERRORS_FOR_ALERT)
							{
								arrTelemetryAlerts.add(currTelemetryData);	//Add to alerts if battery is red low		
								// break since alert is triggered
								break;						
							}
						}
						else if(timeDelta > TelemetryData.TelemetryConstants.ALERT_TIME_INTERVAL)
						{
							// break if timeDelta is 5+ minutes
							break;
						}
					
					}		
				}
				
				
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		return arrTelemetryAlerts;
	}
		
}
	
