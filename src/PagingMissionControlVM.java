import java.util.*;

public class PagingMissionControlVM 
{	
	// Create local objects
	PagingMissionControlModel model;
	ArrayList<TelemetryData> arrTelemetryData;
	
	public PagingMissionControlVM()
	{
		// Instantiate objects
		model = new PagingMissionControlModel();		
	}	
	public void GetTelemetryAlerts()
	{
		arrTelemetryData = model.GetTelemetryData();		
				
		ArrayList<TelemetryData> arrTelemetryAlerts = model.ArrTelemetryDataCheckAlerts(arrTelemetryData);

		System.out.println(model.ArrTelemetryDataToString(arrTelemetryAlerts));		
	}
}
